import {Component} from '@angular/core';
import {HttpClient} from "@angular/common/http";

export class Hero {
    id: number;
    name: string;
}

@Component({
    templateUrl: "/o/department-angular-portlet/lib/app/app.component.html"
})
export class AppComponent {
    hero: Hero = {
        id: 1,
        name: 'Windstorm'
    };
    title = 'Tour of Heroes';

    constructor(private httpClient:HttpClient) {



    }
}
