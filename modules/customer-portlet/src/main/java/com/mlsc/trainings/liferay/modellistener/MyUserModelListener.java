/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mlsc.trainings.liferay.modellistener;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.ModelListener;
import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	service = ModelListener.class
)
public class MyUserModelListener extends BaseModelListener<User> {

	@Override
	public void onBeforeCreate(User model) throws ModelListenerException {
		
		
		/**
		 * 
		 * 
		 */
		
		System.out.println(
			"About to create model: " + model.getPrimaryKey());
		
		
	}
	
	
	@Override
	public void onAfterCreate(User model) throws ModelListenerException {
		
		/**
		 * 
		 * 
		 * 
		 */
		
		System.out.println("The user is created : " + model.getFirstName());
		
		
		super.onAfterCreate(model);
	}
	
}