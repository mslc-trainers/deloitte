package com.mslc.training.liferay.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.WebKeys;
import com.mslc.training.liferay.constants.CustomerPortletKeys;
import com.mslc.training.liferay.model.Customer;
import com.mslc.training.liferay.service.CustomerLocalServiceUtil;
import org.osgi.service.component.annotations.Component;

import javax.portlet.*;
import java.io.IOException;
import java.util.List;


/**
 * OSGI  Based Customer Portlet (jar - OSGI)
 * Plain Portlet Class which happens the OSGI Portlet Service
 * render method
 * - use the value of some parameter to decide on the view to be show. This can be complex
 * action method
 * - We implemented based on the value of cmd, either add or edit
 * <p>
 * Requirements (Architecturally significant requirements)
 * <p>
 * Rendering
 * - I must be able to implement different render functions based on the parameters in the URL
 * - I must not be forced to use PortletRequestDispatcher to forward the request to some jsp
 * - I must be able to work with model
 * <p>
 * Action
 * - I must be able to implement multiple Action methods
 * - Model Objects (I must be able to create Model Classes and bind them with the UI)
 * - I must be able to handle the validations more elegantly.
 */


/**
 * @author muhammed
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.display-category=category.sample",
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.instanceable=false",
                "javax.portlet.display-name=Customer",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.name=" + CustomerPortletKeys.CUSTOMER,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.version=3.0",
                "javax.portlet.security-role-ref=power-user,user"
        },
        service = Portlet.class
)
public class CustomerPortlet extends MVCPortlet {


    /**
     * @param renderRequest  the render request
     * @param renderResponse the render response
     * @throws IOException
     * @throws PortletException
     */

    @Override
    public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

        System.out.println("Render method of Customer Portlet is executed....");

        List<Customer> customers = CustomerLocalServiceUtil.getCustomersByName("IBM");
        customers.forEach(x -> {
            System.out.println(x.getCustomerId() + " -- " + x.getCustomerName() + " -- " + x.getCustomerAddress());
        });
        RenderParameters renderParameters = renderRequest.getRenderParameters();
        String view = renderParameters.getValue("show-view");
        if (view != null && view.equals("customer-form")) {
            PortletRequestDispatcher dispatcher = getPortletContext().getRequestDispatcher("/customer_form.jsp");
            dispatcher.forward(renderRequest, renderResponse);
            return;
        }

        ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
        String resourcePrimaryKey = themeDisplay.getPortletDisplay().getResourcePK();
        long scopeGroupId = themeDisplay.getScopeGroupId();

        PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();

        boolean allowed = permissionChecker.hasPermission(scopeGroupId,
                CustomerPortletKeys.CUSTOMER, resourcePrimaryKey, "ADD_CUSTOMER");
        System.out.println(themeDisplay.getUser().getFullName() + " is allowed to customer : " + allowed);

        renderRequest.setAttribute("addAllowed", allowed);

        super.render(renderRequest, renderResponse);
    }

    @Override
    public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {


        ActionParameters actionParameters = actionRequest.getActionParameters();

        String cmd = actionParameters.getValue(Constants.CMD);
        System.out.println("Action method of Customer Portlet is executed with the value of cmd being : " + cmd);

        Long customerId = Long.valueOf(actionParameters.getValue("customerId"));
        String customerName = actionParameters.getValue("customerName");
        String customerAddress = actionParameters.getValue("customerAddress");

        System.out.println("The values received : " + customerId + " -- " + customerName + " -- " + customerAddress);
        // CustomerLocalServiceUtil.createCustomer
        Customer customer = null;
        try {
            customer = cmd.equals(Constants.ADD)
                    ? CustomerLocalServiceUtil.createCustomer(customerId)
                    : CustomerLocalServiceUtil.getCustomer(customerId);
        } catch (PortalException e) {
            throw new RuntimeException(e);
        }

        customer.setCustomerName(customerName);
        customer.setCustomerAddress(customerAddress);

        CustomerLocalServiceUtil.updateCustomer(customer);

        super.processAction(actionRequest, actionResponse);
    }
}
