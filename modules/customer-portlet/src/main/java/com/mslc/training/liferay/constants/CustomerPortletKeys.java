package com.mslc.training.liferay.constants;

/**
 * @author muhammed
 */
public class CustomerPortletKeys {

	public static final String CUSTOMER =
		"com_mslc_training_liferay_CustomerPortlet";

}