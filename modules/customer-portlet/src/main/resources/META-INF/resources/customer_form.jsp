<%@ page import="com.liferay.portal.kernel.util.Constants" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.mslc.training.liferay.model.Customer" %>
<%@ page import="com.mslc.training.liferay.service.CustomerLocalServiceUtil" %>
<%@ page import="com.mslc.training.liferay.service.persistence.CustomerUtil" %>
<%@ include file="init.jsp" %>

<%--<p>--%>
<%--    <b><liferay-ui:message key="customer.caption"/></b>--%>
<%--</p>--%>
<portlet:renderURL var="renderURL"/>


<%
    //    long customerId = Long.valueOf(request.getParameter("customerId"));
    long customerId = ParamUtil.getLong(request, "customerId", 0);
    String cmd = customerId == 0 ? Constants.ADD : Constants.EDIT;
    Customer customer = cmd.equals(Constants.EDIT)
            ? CustomerLocalServiceUtil.getCustomer(customerId)
            : CustomerUtil.create(0);


%>
<portlet:actionURL var="submitCustomerFormURL">
    <portlet:param name="<%=Constants.CMD%>" value="<%=cmd%>"/>
</portlet:actionURL>


<div class="container-fluid" style="padding: 2rem;">

    <h2>Customer Management</h2>
    <hr>
    <form action="<%=submitCustomerFormURL%>" method="POST">

        <fieldset>

            <div class="form-group">
                <%--                <label for="customerName" path="customerName">--%>
                <%--                    Customer Name--%>
                <%--                </label>--%>
                <input id="<portlet:namespace/>customerId" class="form-control"
                       name="<portlet:namespace/>customerId" path="customerId"   value="<%=customer.getCustomerId()%>"  />

            </div>


            <div class="form-group">
                <%--                <label for="customerName" path="customerName">--%>
                <%--                    Customer Name--%>
                <%--                </label>--%>
                <input id="<portlet:namespace/>customerName" class="form-control"
                       name="<portlet:namespace/>customerName" path="firstName" value="<%=customer.getCustomerName()%>"/>

            </div>

            <div class="form-group">
                <%--                <label for="customerAddress" path="customerAddress">--%>
                <%--                    Customer Address--%>
                <%--                </label>--%>
                <input id="<portlet:namespace/>customerAddress" class="form-control"
                       name="<portlet:namespace/>customerAddress" path="customerAddress" value="<%=customer.getCustomerAddress()%>"/>

            </div>

        </fieldset>
        <div>
            <a href="<%=renderURL%>" class="btn btn-secondary" style="margin-right: 1rem;">Cancel</a> <input
                type="submit" class="btn btn-primary" value="Submit">
        </div>

    </form>
</div>
