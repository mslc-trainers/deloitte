<%@ page import="com.mslc.training.liferay.model.Customer" %>
<%@ page import="com.mslc.training.liferay.service.CustomerLocalServiceUtil" %>
<%@ page import="java.util.List" %>
<%@ page import="javax.portlet.RenderURL" %>
<%@ page import="javax.portlet.MutableRenderParameters" %>
<%@ include file="init.jsp" %>


<%

    List<Customer> customers = CustomerLocalServiceUtil.getCustomers(0, 100);
    boolean addAllowed = (boolean) request.getAttribute("addAllowed");

%>


<portlet:renderURL var="showCustomerFormURL">
    <portlet:param name="show-view" value="customer-form"/>
</portlet:renderURL>


<div class="container-fluid" style="padding: 2rem;">
    <div>
        Hey <%=user.getFullName()%> | <%=themeDisplay.isSignedIn()%>
    </div>
    <br/>
    <h2><liferay-ui:message key="customer.caption"/> [<%=customers.size()%>]</h2>

    <% if (addAllowed) { %>
        <div style="padding-top: 1rem; padding-bottom: 1rem;">
            <a class="btn btn-primary" href="<%=showCustomerFormURL%>">Add Customer</a>
        </div>
    <% } %>

    <hr>
    <div class="row">
        <div class="col">Id</div>
        <div class="col">Name</div>
        <div class="col">Address</div>
        <div class="col">Action</div>
    </div>
    <hr>
    <% for (Customer c : customers) {

        RenderURL customerEditUrl = renderResponse.createRenderURL();
        MutableRenderParameters customerEditParameters = customerEditUrl.getRenderParameters();
        customerEditParameters.setValue("customerId", String.valueOf(c.getCustomerId()));
        customerEditParameters.setValue("show-view", "customer-form");


    %>
    <%--    <portlet:renderURL var="customerEditUrl">--%>
    <%--        <portlet:param name="customerId" value="<%=c.getCustomerId()%>"/>--%>
    <%--    </portlet:renderURL>--%>
    <div class="row">
        <div class="col"><%=c.getCustomerId()%>
        </div>
        <div class="col"><%=c.getCustomerName()%>
        </div>
        <div class="col"><%=c.getCustomerAddress()%>
        </div>
        <div class="col">
            <a href="<%=customerEditUrl%>">Edit</a>
        </div>
    </div>
    <% } %>

</div>
