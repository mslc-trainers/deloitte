package com.mslc.training.liferay.hook;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomizedLoginEvent extends Action {

	@Override
	public void run(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws ActionException {

		long userId = PortalUtil.getUserId(httpServletRequest);

		System.out.println("Login is done by : " + UserLocalServiceUtil.fetchUser(userId));

	}

}
