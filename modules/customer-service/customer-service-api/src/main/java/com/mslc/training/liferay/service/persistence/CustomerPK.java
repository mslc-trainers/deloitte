/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.mslc.training.liferay.service.persistence;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class CustomerPK implements Comparable<CustomerPK>, Serializable {

	public long customerId;
	public String customerName;
	public String customerAddress;

	public CustomerPK() {
	}

	public CustomerPK(
		long customerId, String customerName, String customerAddress) {

		this.customerId = customerId;
		this.customerName = customerName;
		this.customerAddress = customerAddress;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	@Override
	public int compareTo(CustomerPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (customerId < pk.customerId) {
			value = -1;
		}
		else if (customerId > pk.customerId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = customerName.compareTo(pk.customerName);

		if (value != 0) {
			return value;
		}

		value = customerAddress.compareTo(pk.customerAddress);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof CustomerPK)) {
			return false;
		}

		CustomerPK pk = (CustomerPK)object;

		if ((customerId == pk.customerId) &&
			customerName.equals(pk.customerName) &&
			customerAddress.equals(pk.customerAddress)) {

			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, customerId);
		hashCode = HashUtil.hash(hashCode, customerName);
		hashCode = HashUtil.hash(hashCode, customerAddress);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(8);

		sb.append("{");

		sb.append("customerId=");

		sb.append(customerId);
		sb.append(", customerName=");

		sb.append(customerName);
		sb.append(", customerAddress=");

		sb.append(customerAddress);

		sb.append("}");

		return sb.toString();
	}

}