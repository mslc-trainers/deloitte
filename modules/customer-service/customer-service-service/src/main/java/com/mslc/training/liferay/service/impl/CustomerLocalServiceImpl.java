/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.mslc.training.liferay.service.impl;

import com.liferay.portal.aop.AopService;

import com.mslc.training.liferay.model.Customer;
import com.mslc.training.liferay.service.base.CustomerLocalServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

import java.util.List;

/**
 * @author Brian Wing Shun Chan
 */
@Component(
        property = "model.class.name=com.mslc.training.liferay.model.Customer",
        service = AopService.class
)
public class CustomerLocalServiceImpl extends CustomerLocalServiceBaseImpl {


    public List<Customer> getCustomersByName(String name) {
        List<Customer> customers = customerPersistence.findByName(name);

        return customers;
    }
}
