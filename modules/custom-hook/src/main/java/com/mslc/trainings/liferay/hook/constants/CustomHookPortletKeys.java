package com.mslc.trainings.liferay.hook.constants;

/**
 * @author muhammed
 */
public class CustomHookPortletKeys {

	public static final String CUSTOMHOOK =
		"com_mslc_trainings_liferay_hook_CustomHookPortlet";

}