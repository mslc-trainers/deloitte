package com.mlsc.trainings.liferay.rest.application;

import com.mlsc.trainings.liferay.rest.valueobjects.CustomerVO;
import com.mslc.training.liferay.model.Customer;
import com.mslc.training.liferay.service.CustomerLocalServiceUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author muhammed
 */
@Component(
        property = {
                JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/customers",
                JaxrsWhiteboardConstants.JAX_RS_NAME + "=Customers.Rest",
                "auth.verifier.guest.allowed=false"
        },
        service = Application.class
)
public class CustomerRestAPIApplication extends Application {

    public Set<Object> getSingletons() {
        return Collections.<Object>singleton(this);
    }

    @GET
    @Produces("application/json")
    public List<CustomerVO> customers() {


        List<Customer> allCustomers = CustomerLocalServiceUtil.getCustomers(0, 100);

        List<CustomerVO> customers =
                allCustomers
                        .stream()
                        .map(x -> {
                            CustomerVO customerVO = new CustomerVO();
                            customerVO.setCustomerId(x.getCustomerId());
                            customerVO.setCustomerName(x.getCustomerName());
                            customerVO.setCustomerAddress(x.getCustomerAddress());
                            return customerVO;
                        }).collect(Collectors.toList());

        return customers;
    }

//	@GET
//	@Path("/morning")
//	@Produces("text/plain")
//	public String hello() {
//		return "Good morning!";
//	}
//
//	@GET
//	@Path("/morning/{name}")
//	@Produces("text/plain")
//	public String morning(
//		@PathParam("name") String name,
//		@QueryParam("drink") String drink) {
//
//		String greeting = "Good Morning " + name;
//
//		if (drink != null) {
//			greeting += ". Would you like some " + drink + "?";
//		}
//
//		return greeting;
//	}

}
